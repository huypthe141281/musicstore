const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Account = new Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  role: { type: String, default: 3 },
  key: {
    type: "string",
    default: Math.floor(100000 + Math.random() * 900000),
  },
  status: { type: Number, default: 0 },
});
